# librcss.js

This library represents a collection of common utilities, mainly used for monitoring simulation servers and for processing captured log files in the **R**obo**C**up **S**occer **S**imulation domain.  
Its core features are:

- Object representations for 2D and 3D simulation states
- Asynchroneous parsers for *ULG* (2D) and *Replay* (2D / 3D) protocols
- Clients for connecting to a simulation server (proxy) via WebSockets and for loading (potentially gzipped) log files

The aim of this project is to facilitate cross-browser tool development in the RoboCup Soccer Simulation domain.

## Usage

The `createSimClient(ressource)` function provides simple access to the available monitor client implementations of this library. It accepts a `URL` (string) or `File` ressource as input and returns a corresponding `ISimulationClient` implementation.

The API of the `ISimulationClient` can be used synchroneous and asynchroneous.

### Asynchroneous Monitor Client Interface

This code loads a local / remote log file (possibly gzip encoded) using the asynchroneous client API.

```typescript
// loading a local / remote log file
createSimClient('https://some.remote/path').connect()
    .then((sim) => {
        // loading started successfully
        ...
        // wait till the log file was fully loaded
        return sim.closed;
    }).then(() => {
        // loading finished
        ...
    }).catch((err) => {
        // handle errors
    });
```

### Synchroneous Monitor Client Interface

This code connects to a local / remote soccer server instance (via a WebSocket) using the synchroneous client API.
This example also illustrates how to listen to simulation state updates and how a client instance can be used to disconnect again, e.g. by user interaction.

```typescript
// connecting to a web socket stream
let client = createSimClient('wss://some.remote/path');

// register client state listener
client.onStateChange.connect((newState) => {
    if (newState === SimulationClientState.ACTIVE) {
        // client is active --> simulation instance available
        let sim = client.getSimulation();
        
        // listen to simulation state updates
        sim.onStatesChanged.connect(() => { ... });

        ...
    } else if (newState === SimulationClientState.DISCONNECTED) {
        // simulation ended / client disconnected from server
        ...
    }
});

// connect
client.connect();

...

// disconnect (optinal disconnect via user interaction)
client.disconnect();
```
