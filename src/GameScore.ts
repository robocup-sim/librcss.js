/**
 * The GameScore class definition.
 *
 * The GameScore provides information about the current score of a game.
 *
 * @author Stefan Glaser
 */
class GameScore
{
  /** The global time when this score was reached the first time. */
  readonly time: number;

  /** The left team score. */
  readonly goalsLeft: number;

  /** The left team penalty score. */
  readonly penaltyScoreLeft: number;

  /** The left team penalty misses. */
  readonly penaltyMissLeft: number;

  /** The right team score. */
  readonly goalsRight: number;

  /** The right team penalty score. */
  readonly penaltyScoreRight: number;

  /** The right team penalty misses. */
  readonly penaltyMissRight: number;

  
  /** Constant zero game score. */
  static readonly ZERO = new GameScore(0, 0, 0);


  /**
   * GameScore Constructor
   * Create a new GameScore holding the scoring information.
   *
   * @param time
   * @param goalsLeft
   * @param goalsRight
   * @param penScoreLeft
   * @param penMissLeft
   * @param penScoreRight
   * @param penMissRight
   */
  constructor (time: number, goalsLeft: number, goalsRight: number, penScoreLeft: number = 0, penMissLeft: number = 0, penScoreRight: number = 0, penMissRight: number = 0)
  {
    this.time = time;
    this.goalsLeft = goalsLeft;
    this.penaltyScoreLeft = penScoreLeft;
    this.penaltyMissLeft = penMissLeft;
    this.goalsRight = goalsRight;
    this.penaltyScoreRight = penScoreRight;
    this.penaltyMissRight = penMissRight;
  }
}

export { GameScore };
