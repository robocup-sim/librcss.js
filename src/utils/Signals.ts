/**
 * Slot interface, encapsulating a callback function in combination with a context.
 * 
 * @author Stefan Glaser
 */
interface ISlot<T>
{
  /** The slot function. */
  fn: (this: any, payload?: T) => any;

  /** The slot context. */
  ctx: any;
}


/**
 * Interface for representing signals within an application.
 * 
 * @author Stefan Glaser
 */
interface ISignal<T>
{
  /**
   * Connect a slot / callback function to this Signal.
   * 
   * @param slot the slot / callback function
   * @param context the slot context
   * @returns this
   */
  connect(slot: (this: any, payload?: T) => any, context?: any): this;

  /**
   * Disconnect a slot / callback function to this Signal.
   * 
   * @param slot the slot / callback function
   * @param context the slot context
   * @returns this
   */
  disconnect(slot: (this: any, payload?: T) => any, context?: any): this;
}


/**
 * Class for representing signals within an application.
 * 
 * @author Stefan Glaser
 */
class Signal<T> implements ISignal<T>
{
  /** The active slots connected to this signal. */
  slots: ISlot<T>[] = [];

  connect(slot: (this: any, payload?: T) => any, context?: any): this
  {
    typeof slot === 'function' && this.slots.push({ fn: slot, ctx: context || slot });

    return this;
  }

  disconnect(slot: (this: any, payload?: T) => any, context: any = slot): this
  {
    this.slots = this.slots.filter(item => item.fn !== slot && item.ctx !== context);

    return this;
  }

  /**
   * Emit a new signal with the given payload to all connected slots.
   * 
   * @param payload the signal payload
   */
  emit(payload?: T): void
  {
		for (let i = this.slots.length; i--;) {
			let slot: ISlot<T> = this.slots[i];
			slot.fn.call(slot.ctx, payload);
		}
	}
}

export { ISlot, ISignal, Signal };