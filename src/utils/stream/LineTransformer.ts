/**
 * Transformer class for reading a stream line by line.
 * 
 * @author Stefan Glaser
 */
class LineTransformer implements Transformer<string, string>
{
    /** The internal chunk buffer. */
    buffer: string;

    /** The regular expression used to split the data into line tokens. */
    regExp?: RegExp;

    constructor()
    {
        this.buffer = '';
        this.regExp = undefined;
    }

    /**
     * Start a new stream transformation.
     * 
     * @param controller the transform stream controller
     */
    start(controller: TransformStreamDefaultController<string>): any
    {
        this.regExp = undefined;
        this.buffer = '';
    }

    /**
     * Transform the next chunk of data.
     * 
     * @param chunk the next chunk of date to transform
     * @param controller the transform stream controller
     */
    async transform(chunk: string, controller: TransformStreamDefaultController<string>): Promise<void>
    {
        this.buffer += await chunk;

        // lazy create regular expression
        this.regExp ??= new RegExp('[^\r\n]+', 'g');

        let lastIdx = this.regExp.lastIndex;
        let tokens = this.regExp.exec(this.buffer);

        while (this.regExp.lastIndex !== 0 && this.regExp.lastIndex !== this.buffer.length) {
            if (!!tokens && tokens.length > 0) {
                controller.enqueue(tokens[0]);
            }

            lastIdx = this.regExp.lastIndex;
            tokens = this.regExp.exec(this.buffer);
        }

        this.buffer = this.buffer.slice(lastIdx);
        this.regExp.lastIndex = 0;
    }

    /**
     * Finish the transformation of the existing stream.
     * 
     * @param controller the transform stream controller
     */
    flush(controller: TransformStreamDefaultController<string>): void
    {
        let tokens = this.regExp?.exec(this.buffer);
        if (!!tokens && tokens.length > 0) {
            controller.enqueue(tokens[0]);
        }

        // clear transformer
        this.regExp = undefined;
        this.buffer = '';
    }
}


/**
 * Line Transform Stream.
 * 
 * @author Stefan Glaser
 */
class LineTransformStream extends TransformStream<string, string>
{
    constructor()
    {
        super(new LineTransformer());
    }
}

export { LineTransformer, LineTransformStream };