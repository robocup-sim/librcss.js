/**
 * Simple interface for a 3D vector
 */
interface IVector3D {
  x: number;
  y: number;
  z: number;
}


/**
 * Simple interface for a quaternion.
 */
interface IQuaternion {
  x: number;
  y: number;
  z: number;
  w: number;
}


/**
* Simple math helpers.
* 
* @author Stefan Glaser
*/
class JsMath
{
  /**
  * Transform degrees to radians.
  * @param deg the angle in degrees
  * @returns the angle in rad
  */
  static toRad (deg: number): number
  {
    return deg * Math.PI / 180.0;
  }

  /**
  * Transform radians to degrees.
  * @param rad the angle in rad
  * @returns the angle in degrees
  */
  static toDeg (rad: number): number
  {
    return rad * 180.0 / Math.PI;
  }

  /**
  * Calcuate a Quaternion representation for a given horizontal angle.
  * 
  * @param angle the horizontal angle in radian
  * @returns a IQuationion representing the rotation in 3D space
  */
  static get2DQuaternion(angle: number): IQuaternion
  {
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm

    // assumes axis is normalized
    const axisX = 0;
    const axisY = 0;
    const axisZ = 1;

    const halfAngle = angle / 2;
    const s = Math.sin( halfAngle );

    return {
      x: axisX * s,
      y: axisY * s,
      z: axisZ * s,
      w: Math.cos( halfAngle )
    };
  }
}

export { IVector3D, IQuaternion, JsMath };
  