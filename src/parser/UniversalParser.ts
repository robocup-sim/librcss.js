import { IMonitorProtocolParser, ParserError } from './MonitorProtocolParser.js';
import { Simulation } from '../Simulation.js';
import { LineTransformStream } from '../utils/stream/LineTransformer.js';
import { ULGParser } from './sserver/ULGParser.js';
import { ReplayParser } from './replay/ReplayParser.js';

/**
 * The UniveralParser class definition.
 *
 * The UniveralParser acts as a wrapper for all available simulation parsers.
 * The decision for the actual parser is based on known header information, read from the provided simulation stream.
 *
 * @author Stefan Glaser
 */
class UniversalParser implements IMonitorProtocolParser
{    
  /** The actual parser instance. */
  private parser?: IMonitorProtocolParser = undefined;

  constructor ()
  {
    // console.log('New universal simulation parser instance created!');
  }

  async parse(stream: ReadableStream<string>, ressource: URL | File): Promise<Simulation>
  {
    const streamTees = stream.tee();
    const testStream = streamTees[0];
    const parserStream = streamTees[1];

    // read header of test stream
    const reader = testStream.pipeThrough(new LineTransformStream()).getReader();

    const { done, value } = await reader.read();

    if (done || !value) {
      throw new ParserError('Failed parsing simulation - empty stream!');
    }

    // create actual parser isntance
    this.parser = value.startsWith('ULG') ? new ULGParser() : new ReplayParser();

    // cancel test stream
    reader.cancel();

    // forward parsing
    return await this.parser.parse(parserStream, ressource);
  }
}

export { UniversalParser };
