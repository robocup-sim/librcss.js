import { Simulation } from '../Simulation.js';
import { WorldState } from '../WorldState.js';
import { PartialWorldState } from './PartialWorldState.js';

/**
 * The ParserStorage class definition.
 *
 * @author Stefan Glaser
 */
class ParserStorage
{
  /** The partial state used during parsing. */
  partialState?: PartialWorldState;

  /** The internal state buffer. */
  stateBuffer: Array<WorldState>;

  /** The maximum states to parse per run. */
  maxStates: number;

  /** The index list for the recent player types of the individual agents of the left team. */
  leftIndexList: number[];

  /** The index list for the recent player types of the individual agents of the right team. */
  rightIndexList: number[];

  constructor ()
  {
    this.partialState = undefined;
    this.stateBuffer = [];
    this.maxStates = 500;
    this.leftIndexList = [];
    this.rightIndexList = [];
  }

  /**
   * Check if a partial state instance exists.
   *
   * @returns true, if the partial state exists, false otherwise
   */
  hasPartialState (): boolean
  {
    return !!this.partialState;
  }

  /**
   * Store the partial state (if valid) in the internal state buffer and reset the partial state afterwards.
   * 
   * @returns true, if a valid state has been added to the state buffer, false otherwise
   */
  storePartialState (): boolean
  {
    return this.partialState?.appendTo(this.stateBuffer) ?? false;
  }

  /**
   * Check if we reched the state chunk limit.
   * 
   * @returns true, if the state buffer length is greater or equal the specified chunk size, false otherwise
   */
  hasReachedStateLimit (): boolean
  {
    return this.stateBuffer.length >= this.maxStates;
  }
  
  /**
   * Retrieve the state buffer.
   * This method also resets the internal state buffer of the parser storage.
   * 
   * @returns the buffered world states
   */
  consumeStateBuffer (): Array<WorldState>
  {
    const buffer = this.stateBuffer;
    this.stateBuffer = [];
    return buffer;
  }
}

export { ParserStorage };
