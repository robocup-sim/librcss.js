import { IMonitorProtocolParser, ParserError, copyString, aSleep } from '../MonitorProtocolParser.js';
import { AgentState } from '../../AgentState.js';
import { ParserStorage } from '../ParserStorage.js';
import { ObjectState } from '../../ObjectState.js';
import { SimulationType, Simulation } from '../../Simulation.js';
import { PartialWorldState } from '../PartialWorldState.js';
import { Replay } from './Replay.js';
import { LineTransformStream } from '../../utils/stream/LineTransformer.js';
import { SparkUtil } from '../../utils/SparkUtil.js';
import { ParameterMap, ParameterObject } from '../../utils/ParameterMap.js';
import { IQuaternion, IVector3D, JsMath } from '../../utils/JsMath.js';
import { SymbolNode } from '../../utils/symboltree/SymbolNode.js';
import { SymbolTreeParser } from '../../utils/symboltree/SymbolTreeParser.js';
import { Agent2DFlags, Agent2DData } from '../../utils/SServerUtil.js';

/**
 * The ReplayParser class definition.
 *
 * The ReplayParser provides
 *
 * @author Stefan Glaser
 */
class ReplayParser implements IMonitorProtocolParser
{
  /** The replay. */
  replay?: Replay = undefined;

  /** The storage instance used during parsing. */
  storage?: ParserStorage = undefined;

  /** The monitor data stream reader. */
  reader?: ReadableStreamDefaultReader<string> = undefined;

  constructor ()
  {
    // console.log('New Replay parser instance created!');
  }

  async parse(stream: ReadableStream<string>, ressource: URL | File): Promise<Simulation>
  {
    // fetch reader instance
    this.reader = stream.pipeThrough(new LineTransformStream()).getReader();

    let { done, value } = await this.reader.read();

    if (done || !value) {
      throw new ParserError('Failed parsing Replay stream - empty stream!');
    }

    // Start parsing
    let splitLine = value.split(' ');

    if (value.charAt(0) === 'R' &&
        value.charAt(1) === 'P' &&
        value.charAt(2) === 'L') {
      // Found replay header
      if (splitLine.length < 3) {
        throw new ParserError('Malformated Replay Header!');
      }

      const type = splitLine[1] === '2D' ? SimulationType.TWOD : SimulationType.THREED;
      this.replay = new Replay(ressource, type, parseInt(splitLine[2], 10));
    } else {
      // No replay header found, try fallback...
      if (value.charAt(0) === 'T') {
        // Initial 2D replay format, use fallback parser
        console.log('ReplayParser: Detected old 2D replay file format!');

        // Parse teams line
        if (splitLine.length < 3) {
          throw new ParserError('Invalid team line!');
        }

        this.replay = new Replay(ressource, SimulationType.TWOD, 0);
        this.replay.leftTeam.name = copyString(splitLine[1].slice(1, -1));
        this.replay.rightTeam.name = copyString(splitLine[2].slice(1, -1));

        // Create default agents with numbers 1 to 11 for both sides
        for (let i = 1; i < 12; i++) {
          this.replay.leftTeam.addAgent(i, this.replay.playerTypes[0]);
          this.replay.rightTeam.addAgent(i, this.replay.playerTypes[0]);
        }
      } else if (value.charAt(0) === 'V') {
        // Initial 3D replay format, use fallback parser
        console.log('ReplayParser: Detected old 3D replay file format!');

        if (splitLine.length < 4) {
          throw new ParserError('Malformated Replay Header!');
        }

        this.replay = new Replay(ressource, SimulationType.THREED, 0);
        this.replay.frequency = parseInt(splitLine[3], 10);

        // Parse teams line
        ({ done, value } = await this.reader.read());
        if (done || !value) {
          throw new ParserError('Replay corrupt!');
        }
        splitLine = value.split(' ');
        if (splitLine.length < 5 || splitLine[0] != 'T') {
          throw new ParserError('Invalid teams line!');
        }

        this.replay.leftTeam.name = copyString(splitLine[1].slice(1, -1));
        this.replay.rightTeam.name = copyString(splitLine[3].slice(1, -1));
        try {
          this.replay.leftTeam.color = copyString(splitLine[2]);
          this.replay.rightTeam.color = copyString(splitLine[4]);
        } catch (ex) {
          console.log(ex);
        }

        // Parse world line
        ({ done, value } = await this.reader.read());
        if (done || !value) {
          throw new ParserError('Replay corrupt!');
        }
        splitLine = value.split(' ');
        if (splitLine.length < 2 || splitLine[0] != 'F') {
          throw new ParserError('Invalid world line!');
        }

        // Extract field parameters based on server version
        switch (parseInt(splitLine[1], 10)) {
          case 62:
            this.replay.environmentParams = SparkUtil.createEnvironmentParamsV62();
            break;
          case 63:
            this.replay.environmentParams = SparkUtil.createEnvironmentParamsV63();
            break;
          case 64:
            this.replay.environmentParams = SparkUtil.createEnvironmentParamsV64();
            break;
          case 66:
            this.replay.environmentParams = SparkUtil.createEnvironmentParamsV66();
            break;
          default:
            break;
        }
      } else {
        throw new ParserError('Failed parsing replay file - no Replay header found (and none of the fallback options applies)!');
      }
    }

    this.storage = new ParserStorage();
    // use 10 intervals for log data
    this.storage.maxStates = this.replay.type === SimulationType.TWOD ? 250 : 100;
    if (!this.replay.isLog()) {
      // use 1 second intervals for stream data
      this.storage.maxStates /= 10;
    }

    this.replay.closed = this.parseBody();

    // return simulation
    return this.replay;
  }

  private async parseBody()
  {
    // console.log('Parsing replay body...');
    if (!this.reader || !this.replay || !this.storage) {
      throw new ParserError('Parser not initialized. Need to parse the header first!');
    }
  
    // Parsing functions
    let parseBallFcn = parseBallState_2D;
    let parseAgentFcn = parseAgentState_V0_2D;
    if (this.replay.type === SimulationType.THREED) {
      if (this.replay.version === 0) {
        parseBallFcn = parseBallState_V0_3D;
        parseAgentFcn = parseAgentState_V0_3D;
      } else {
        parseBallFcn = parseBallState_V1_3D;
        parseAgentFcn = parseAgentState_V1;
      }
    } else if (this.replay.version > 0) {
      parseAgentFcn = parseAgentState_V1;
    }
    
    // temporary yield event loop before beginning actual paring
    await aSleep();

    while (true) {
      const { done, value } = await this.reader.read();

      if (done) {
        break;
      }

      const dataLine = value;
      try {
        switch (dataLine.charAt(0)) {
          case 'E': // Environment parameter line
            if (dataLine.charAt(1) === 'P') {
              parseEnvironmentParams(dataLine, this.replay, this.storage);
            }
            break;
          case 'P':
            if (dataLine.charAt(1) === 'P') {
              // Player parameter line
              parsePlayerParams(dataLine, this.replay);
            } else if (dataLine.charAt(1) === 'T') {
              // Player type line
              parsePlayerTypeParams(dataLine, this.replay);
            }
            break;
          case 'T': // Team info line
            parseTeamLine(dataLine, this.replay);
            break;
          case 'S': // State data line
            parseStateLine(dataLine, this.replay, this.storage)
            break;
  
          case 'b': // Ball data line
            parseBallFcn(dataLine, this.storage.partialState);
            break;
  
          case 'l': // Left agent data line
          case 'L':
            parseAgentFcn(dataLine, this.replay, this.storage, true);
            break;
  
          case 'r': // Right agent data line
          case 'R':
            parseAgentFcn(dataLine, this.replay, this.storage, false);
            break;
        }
      } catch (err) {
        console.log(err);
      }

      if (this.storage.hasReachedStateLimit()) {
        this.replay.update(this.storage.consumeStateBuffer(), true);

        // temporary yield event loop
        await aSleep();
      }
    }

    // push final states
    this.storage.storePartialState();
    this.replay.update(this.storage.consumeStateBuffer(), false);

    // reset
    this.reader.releaseLock();
    this.reader = undefined;
    this.replay = undefined;
    this.storage = undefined;
  }

  /**
   * Check if the given url/path/file references a known replay file ending.
   *
   * @param url the url to check
   * @returns true, if the given url references a known replay file ending, false otherwise
   */
  static isReplayFile (url: string): boolean
  {
    return url.endsWith('.rpl3d') || url.endsWith('.rpl2d') || url.endsWith('.replay')
      || url.endsWith('.rpl3d.gz') || url.endsWith('.rpl2d.gz') || url.endsWith('.replay.gz');
  }
}

export { ReplayParser };






// ============================================================================
// ======================== PRIVATE PARSING FUNCTIONS =========================
// ============================================================================

// ----------------------------------------------------------------------------
// --------------------------------- GENERAL ----------------------------------
// ----------------------------------------------------------------------------

/**
 * Parse a environment parameter line.
 *
 * @param dataLine the environment params line
 * @param replay the replay instance
 * @param storage the parser storage instance
 */
function parseEnvironmentParams (dataLine: string, replay: Replay, storage: ParserStorage): void
{
  // Environment-Parameter Line-Format:
  // EP <single-line-json>
  try {
    const newParams = JSON.parse(dataLine.slice(3)) as ParameterObject;
    replay.environmentParams.clear();
    replay.environmentParams.paramObj = newParams;
  } catch (ex) {
    console.log('Exception while parsing environment parameters:');
    console.log(ex);
  }

  // Update replay frequency and partial state time step
  replay.updateFrequency();
  if (storage.partialState) {
    storage.partialState.timeStep = 1 / replay.frequency;
  }
}

/**
 * Parse a player parameter line.
 *
 * @param dataLine the player params line
 * @param replay the replay instance
 */
function parsePlayerParams (dataLine: string, replay: Replay): void
{
  // Player-Parameter Line-Format:
  // PP <single-line-json>
  try {
    const newParams = JSON.parse(dataLine.slice(3)) as ParameterObject;
    replay.playerParams.clear();
    replay.playerParams.paramObj = newParams;
  } catch (ex) {
    console.log('Exception while parsing player parameters:');
    console.log(ex);
  }
}

/**
 * Parse a player type parameter line.
 *
 * @param dataLine the player params line
 * @param replay the replay instance
 */
function parsePlayerTypeParams (dataLine: string, replay: Replay): void
{
  // Player-Type-Parameter Line-Format:
  // PT <id> <single-line-json>
  const idx = dataLine.indexOf(' ', 4);

  if (idx > 3 && idx < 10) {
    const typeIdx = parseInt(dataLine.slice(3, idx), 10);

    try {
      replay.playerTypes[typeIdx] = new ParameterMap(JSON.parse(dataLine.slice(idx + 1)));
    } catch (ex) {
      console.log('Exception while parsing player type parameters:');
      console.log(ex);
    }
  }
}

/**
 * Parse a team info line.
 *
 * @param dataLine the team info line
 * @param replay the replay to store the parsed states
 */
function parseTeamLine (dataLine: string, replay: Replay): void
{
  // Teams-Line-Format:
  // T <left-team> <right-team>[ <left-color <right-color>]
  const line = dataLine.split(' ');
  if (line.length < 3) {
    // Not enough data!
    return;
  }

  replay.leftTeam.name = copyString(line[1]);
  replay.rightTeam.name = copyString(line[2]);

  if (line.length > 4) {
    try {
      replay.leftTeam.color = copyString(line[3]);
      replay.rightTeam.color = copyString(line[4]);
    } catch (ex) {
      console.log(ex);
    }
  }
}

/**
 * Parse a state info line.
 *
 * @param dataLine the team info line
 * @param replay the replay to store the parsed states
 * @param storage the parser storage instance
 * @returns true, if a new world state was created, false otherwise
 */
function parseStateLine (dataLine: string, replay: Replay, storage: ParserStorage): void
{
  // State-Line-Format:
  // S <game-time> <playmode> <score-left> <score-right>[ <penalty-score-left> <penalty-miss-left> <penalty-miss-right> <penalty-miss-right>]
  const line = dataLine.split(' ');
  if (line.length < 5) {
    // Not enough data!
    return;
  }

  // lazy create the first partial state when frequency information is available
  storage.partialState ??= new PartialWorldState(0, 1 / replay.frequency, 0);

  // new state line --> previous state information block ended
  storage.storePartialState();

  let gameTime = parseFloat(line[1]);
  if (replay.version === 0) {
    gameTime /= 10;
  }

  storage.partialState.setGameTime(gameTime);
  storage.partialState.setPlaymode(copyString(line[2]));

  if (line.length > 8) {
    storage.partialState.setScore(
        parseInt(line[3], 10),
        parseInt(line[4], 10),
        parseInt(line[5], 10),
        parseInt(line[6], 10),
        parseInt(line[7], 10),
        parseInt(line[8], 10)
      );
  } else {
    storage.partialState.setScore(parseInt(line[3], 10), parseInt(line[4], 10));
  }
}

/**
 * [parseBallState description]
 *
 * @param dataLine
 * @param partialState
 */
function parseBallState_2D (dataLine: string, partialState?: PartialWorldState): void
{
  // Ball-Line-Format:
  // b <x> <y>
  const line = dataLine.split(' ');
  if (!partialState || line.length < 3) {
    // Not enough data!
    return;
  }

  partialState.ballState = new ObjectState({
      x: parseFloat(line[1]),
      y: parseFloat(line[2]),
      z: 0.2,
      qx: 0,
      qy: 0,
      qz: 0,
      qw: 1
    });
}











// ----------------------------------------------------------------------------
// --------------------------------- VERSION 0 --------------------------------
// ----------------------------------------------------------------------------



/**
 * [parseBallState description]
 *
 * @param dataLine
 * @param partialState
 */
function parseBallState_V0_3D (dataLine: string, partialState?: PartialWorldState): void
{
  // Ball-Line-Format:
  // b <x> <y> <z> <qx> <qy> <qz> <qw>
  const line = dataLine.split(' ');
  if (!partialState || line.length < 8) {
    // Not enough data!
    return;
  }

  partialState.ballState = new ObjectState({
      x: parseInt(line[1], 10) / 1000,
      y: parseInt(line[2], 10) / 1000,
      z: parseInt(line[3], 10) / 1000,
      qx: parseInt(line[5], 10) / 1000,
      qy: parseInt(line[6], 10) / 1000,
      qz: parseInt(line[7], 10) / 1000,
      qw: parseInt(line[4], 10) / 1000
    });
}

/**
 * [parseAgentState description]
 *
 * @param dataLine the agent line
 * @param replay the replay to store the parsed states
 * @param storage the parser storage instance
 * @param leftSide side indicator
 */
function parseAgentState_V0_2D (dataLine: string, replay: Replay, storage: ParserStorage, leftSide: boolean): void
{
  // Agent-Line-Format:
  // {l|L|r|R} <unum> <x> <y> <heading-angle>[ <neck-angle> <stamina>]
  const line = dataLine.split(' ');
  if (!storage.partialState || line.length < 5) {
    // Not enough data!
    return;
  }

  const playerNo = parseInt(line[1], 10);
  let flags = Agent2DFlags.STAND;

  // Check for goalie
  if (line[0] === 'L' || line[0] === 'R') {
    flags |= Agent2DFlags.GOALIE;
  }

  // Parse player state data
  let angle = parseFloat(line[4]);
  const quat = JsMath.get2DQuaternion(JsMath.toRad(angle));
  const jointData:number[] = [];
  const agentData:number[] = [];

  if (line.length > 6) {
    angle = parseFloat(line[5]) - angle;
    if (angle > 180) {
      angle -= 360;
    } else if (angle < -180) {
      angle += 360;
    }

    jointData[0] = JsMath.toRad(-angle);
    agentData[Agent2DData.STAMINA] = parseFloat(line[6].slice(1));
  }

  const newState = new AgentState({
      modelIdx: 0,
      flags: flags,
      x: parseFloat(line[2]),
      y: parseFloat(line[3]),
      z: 0,
      qx: quat.x,
      qy: quat.y,
      qz: quat.z,
      qw: quat.w,
      jointAngles: jointData,
      data: agentData
    });

  if (leftSide) {
    storage.partialState.leftAgentStates[playerNo] = newState;
  } else {
    storage.partialState.rightAgentStates[playerNo] = newState;
  }
}

/**
 * [parseAgentState description]
 *
 * @param dataLine the agent line
 * @param replay the replay to store the parsed states
 * @param storage the parser storage instance
 * @param leftSide side indicator
 */
function parseAgentState_V0_3D (dataLine: string, replay: Replay, storage: ParserStorage, leftSide: boolean): void
{
  // Agent-Line-Format:
  // {l|L|r|R} <unum>[ model] <x> <y> <z> <qx> <qy> <qz> <qr> [ <joint-angle>]*
  const line = dataLine.split(' ');
  if (!storage.partialState || line.length < 9) {
    // Not enough data!
    return;
  }

  const playerNo = parseInt(line[1], 10);
  let dataIdx = 2;
  let modelIdx = 0;
  let team;
  let indexList;
  let agentStates;

  if (leftSide) {
    team = replay.leftTeam;
    indexList = storage.leftIndexList;
    agentStates = storage.partialState.leftAgentStates;
  } else {
    team = replay.rightTeam;
    indexList = storage.rightIndexList;
    agentStates = storage.partialState.rightAgentStates;
  }

  // Check for model definition
  if (line[0] === 'L' || line[0] === 'R') {
    if (line.length < 10) {
      // Not enough data!
      return;
    }

    dataIdx++;

    let modelType = 0;
    try {
      modelType = parseInt(line[2].slice(-1), 10);
    } catch (err) {
    }

    team.addAgent(playerNo, replay.playerTypes[modelType]);
    indexList[playerNo] = team.getRecentTypeIdx(playerNo);
  }

  if (indexList[playerNo] !== undefined) {
    modelIdx = indexList[playerNo];
  }

  // Parse player state data
  const px = parseInt(line[dataIdx], 10) / 1000;
  const py = parseInt(line[dataIdx + 1], 10) / 1000;
  const pz = parseInt(line[dataIdx + 2], 10) / 1000;

  const qx = parseInt(line[dataIdx + 4], 10) / 1000;
  const qy = parseInt(line[dataIdx + 5], 10) / 1000;
  const qz = parseInt(line[dataIdx + 6], 10) / 1000;
  const qw = parseInt(line[dataIdx + 3], 10) / 1000;
  const jointData:number[] = [];
  dataIdx += 7;

  // Shuffle joint data
  // Old joint order: <head> <l-arm> <l-leg> <r-arm> <r-leg>
  // New joint order: <head> <r-arm> <l-arm> <r-leg> <l-leg>
  let i;
  const numLegJoints = line.length - dataIdx > 22 ? 7 : 6;
  const lArmData = [];
  const lLegData = [];

  for (i = 0; i < 2 && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(JsMath.toRad(parseFloat(line[dataIdx]) / 100));
  }
  for (i = 0; i < 4 && dataIdx < line.length; i++, dataIdx++) {
    lArmData.push(JsMath.toRad(parseFloat(line[dataIdx]) / 100));
  }
  for (i = 0; i < numLegJoints && dataIdx < line.length; i++, dataIdx++) {
    lLegData.push(JsMath.toRad(parseFloat(line[dataIdx]) / 100));
  }
  for (i = 0; i < 4 && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(JsMath.toRad(parseFloat(line[dataIdx]) / 100));
  }
  for (i = 0; i < lArmData.length; i++) {
    jointData.push(lArmData[i]);
  }
  for (i = 0; i < numLegJoints && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(JsMath.toRad(parseFloat(line[dataIdx]) / 100));
  }
  for (i = 0; i < lLegData.length; i++) {
    jointData.push(lLegData[i]);
  }


  agentStates[playerNo] = new AgentState({
      modelIdx: modelIdx,
      flags: 0x00,
      x: px,
      y: py,
      z: pz,
      qx: qx,
      qy: qy,
      qz: qz,
      qw: qw,
      jointAngles: jointData,
      data: []
    });
}











// ----------------------------------------------------------------------------
// --------------------------------- VERSION 1 --------------------------------
// ----------------------------------------------------------------------------

/**
 * [parseBallState description]
 *
 * @param dataLine
 * @param partialState
 */
function parseBallState_V1_3D (dataLine: string, partialState?: PartialWorldState): void
{
  // Ball-Line-Format:
  // b <x> <y> <z> <qx> <qy> <qz> <qw>
  const line = dataLine.split(' ');
  if (!partialState || line.length < 8) {
    // Not enough data!
    return;
  }

  partialState.ballState = new ObjectState({
      x: parseFloat(line[1]),
      y: parseFloat(line[2]),
      z: parseFloat(line[3]),
      qx: parseFloat(line[5]),
      qy: parseFloat(line[6]),
      qz: parseFloat(line[7]),
      qw: parseFloat(line[4])
    });
}

/**
 * [parseAgentState description]
 *
 * @param dataLine the agent line
 * @param replay the replay to store the parsed states
 * @param storage the parser storage instance
 * @param leftSide side indicator
 */
function parseAgentState_V1 (dataLine: string, replay: Replay, storage: ParserStorage, leftSide: boolean): void
{
  // Agent-Line-Format:
  // 2D:
  // {l|L|r|R} <unum>[ typeIdx] <flags> <x> <y> <heading-angle>[(j[ <joint-angle>]+)][(s <stamina>)]
  // 3D:
  // {l|L|r|R} <unum>[ typeIdx] <flags> <x> <y> <z> <qx> <qy> <qz> <qr>[(j[ <joint-angle>]+)][(s <stamina>)]
  const rootNode = SymbolTreeParser.parse('(' + dataLine + ')');
  if (!storage.partialState || rootNode.values.length < 6) {
    // Not enough data!
    return;
  }

  const playerNo = parseInt(rootNode.values[1], 10);
  let dataIdx = 2;
  let modelIdx = 0;
  let team;
  let indexList;
  let agentStates;

  if (leftSide) {
    team = replay.leftTeam;
    indexList = storage.leftIndexList;
    agentStates = storage.partialState.leftAgentStates;
  } else {
    team = replay.rightTeam;
    indexList = storage.rightIndexList;
    agentStates = storage.partialState.rightAgentStates;
  }

  // Check for model definition
  if (rootNode.values[0] === 'L' || rootNode.values[0] === 'R') {
    team.addAgent(playerNo, replay.playerTypes[parseInt(rootNode.values[dataIdx], 10)]);
    indexList[playerNo] = team.getRecentTypeIdx(playerNo);
    dataIdx++;
  }

  if (indexList[playerNo] !== undefined) {
    modelIdx = indexList[playerNo];
  }

  const flags = parseInt(rootNode.values[dataIdx], 16);
  dataIdx++;

  // Parse player state data
  const position: IVector3D = {x: 0, y: 0, z: 0};
  let quat: IQuaternion = {x: 0, y: 0, z: 0, w: 1};
  const jointData:number[] = [];
  const agentData:number[] = [];
  const is2D = replay.type === SimulationType.TWOD;

  if (is2D) {
    position.x = parseFloat(rootNode.values[dataIdx]);
    position.y = parseFloat(rootNode.values[dataIdx + 1]);
    position.z = 0;

    quat = JsMath.get2DQuaternion(JsMath.toRad(-1 * parseFloat(rootNode.values[dataIdx + 2])));
  } else {
    position.x = parseFloat(rootNode.values[dataIdx]);
    position.y = parseFloat(rootNode.values[dataIdx + 1]);
    position.z = parseFloat(rootNode.values[dataIdx + 2]);
    quat.x = parseFloat(rootNode.values[dataIdx + 4]);
    quat.y = parseFloat(rootNode.values[dataIdx + 5]);
    quat.z = parseFloat(rootNode.values[dataIdx + 6]);
    quat.w = parseFloat(rootNode.values[dataIdx + 3]);
  }

  for (let i = 0; i < rootNode.children.length; i++) {
    switch (rootNode.children[i].values[0]) {
      case 'j':
        parseJointNode(rootNode.children[i], jointData, is2D);
        break;
      case 's':
        agentData[Agent2DData.STAMINA] = parseFloat(rootNode.children[i].values[1]);
        break;
      default:
        break;
    }
  }

  agentStates[playerNo] = new AgentState({
      modelIdx: modelIdx,
      flags: flags,
      x: position.x,
      y: position.y,
      z: position.z,
      qx: quat.x,
      qy: quat.y,
      qz: quat.z,
      qw: quat.w,
      jointAngles: jointData,
      data: agentData
    });
}

/**
 * Parse a joint-angles symbol node into a joint-data array.
 *
 * @param node the joint-angles symbol node
 * @param jointData the joint data list
 * @param convert indicator if joint angles sign should be negated
 */
function parseJointNode (node: SymbolNode, jointData: number[], convert: boolean = false): void
{
  const factor = convert ? -1 : 1;

  for (let i = 1; i < node.values.length; i++) {
    jointData.push(factor * JsMath.toRad(parseFloat(node.values[i])));
  }
}
