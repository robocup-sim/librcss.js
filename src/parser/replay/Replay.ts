import { SimulationType, Simulation } from '../../Simulation.js';

/**
 * The Replay class definition.
 *
 * The Replay is the central class holding a replay.
 *
 * @author Stefan Glaser
 */
class Replay extends Simulation
{
  /** The replay protocol version. */
  version: number;

  /**
   * Replay Constructor
   * Create a new replay.
   * 
   * @param ressource the simulation ressource
   * @param type the simulation type
   * @param version the replay version
   */
  constructor (ressource: URL | File, type: SimulationType, version: number)
  {
    super(ressource, type);

    this.version = version;
  }
}

export { Replay };
