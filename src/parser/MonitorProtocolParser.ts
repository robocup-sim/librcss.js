import { Simulation } from '../Simulation.js';


/**
 * The IMonitorProtocolParser interface definition.
 *
 * @author Stefan Glaser
 */
interface IMonitorProtocolParser
{
  /**
   * Parse the given monitor data stream into a {@link Simulation} data structure.
   * 
   * Parsing is divided into two stages:
   * 1. Parsing header information --> 2D or 3D simulation
   * 2. Continuously parsing new simulation data / states
   * 
   * This method resolves after the first stage (parsing the header information).
   * The second stage is encapsulated withing the "closed" promise of the simulation.
   * 
   * The parser will automatically clean up itself after it reached the end of the stream.
   * 
   * @param stream the simulation monitor data stream
   * @param ressource the stream ressource
   */
  parse (stream: ReadableStream<string>, ressource: URL | File): Promise<Simulation>;
}


/**
 * Simple class for signaling parser errrors.
 * 
 * @author Stefan Glaser
 */
class ParserError extends Error
{
  /**
   * ParserError Constructor
   *
   * @param msg the exception message
   */
  constructor (msg: string)
  {
    super(msg);
    this.name = 'ParserError';
  }
}


/**
 * The copyString funcion presents a workaround for deep copying partial strings.
 *
 * Modern browsers only provide partial strings when using string.substring() / .slice() / etc.
 * while keeping a reference to the original string. While this usually improves the overall
 * performance and memory consumption, it also prevents the garbage collector from collecting
 * the original string. This function provides a workaround for really copying a string value
 * (obtained via .substring() / .slice() / etc.).
 * Use this function when storing partial strings in your result objects.
 *
 * @param partialString
 * @return a "deep" copy of the above partial string
 */
function copyString (partialString: string): string
{
  if (partialString) {
    return JSON.parse(JSON.stringify(partialString)) as string;
  }

  return partialString;
}


/**
 * Asynchronous sleep, returning a promise resolving after the specified time.
 * 
 * @param ms the time to sleep in milliseconds
 * @returns a promise resolving after the specified time
 */
function aSleep (ms: number = 0)
{
  return new Promise<void>((resolve, fail) => setTimeout(resolve, ms));
}

export { IMonitorProtocolParser, ParserError, copyString, aSleep};
