import { IMonitorProtocolParser, ParserError, copyString, aSleep } from '../MonitorProtocolParser.js';
import { AgentState } from '../../AgentState.js';
import { ParserStorage } from '../ParserStorage.js';
import { ObjectState } from '../../ObjectState.js';
import { Simulation } from '../../Simulation.js';
import { SServerLog } from './SServerLog.js';
import { SymbolNode } from '../../utils/symboltree/SymbolNode.js';
import { SymbolTreeParser } from '../../utils/symboltree/SymbolTreeParser.js';
import { TeamDescription } from '../../TeamDescription.js';
import { LineTransformStream } from '../../utils/stream/LineTransformer.js';
import { ParameterObject, ParameterMap } from '../../utils/ParameterMap.js';
import { IQuaternion, JsMath } from '../../utils/JsMath.js';
import { PartialWorldState } from '../PartialWorldState.js';
import { Agent2DData, PlayerType2DParams } from '../../utils/SServerUtil.js';

/**
 * The ULGParser class definition.
 *
 * The ULGParser provides
 *
 * @author Stefan Glaser
 */
class ULGParser implements IMonitorProtocolParser
{
  /** The sserver log. */
  sserverLog?: SServerLog = undefined;

  /** The storage instance used during parsing. */
  storage?: ParserStorage = undefined;

  /** The monitor data stream reader. */
  reader?: ReadableStreamDefaultReader<string> = undefined;

  constructor()
  {
    // console.log('New USG parser instance created!');
  }

  async parse(stream: ReadableStream<string>, ressource: URL | File): Promise<Simulation>
  {
    // fetch reader instance
    this.reader = stream.pipeThrough(new LineTransformStream()).getReader();

    const { done, value } = await this.reader.read();

    // ==================== Check ULG Header ====================
    if (done || !value) {
      throw new ParserError('Failed parsing ULG stream - empty stream!');
    } else if (!value.startsWith('ULG')) {
      throw new ParserError('Failed parsing ULG stream - no ULG header found!');
    }
    const ulgVersion = parseInt(value.slice(3), 10);
    this.sserverLog = new SServerLog(ressource, ulgVersion);
    this.storage = new ParserStorage();
    this.storage.partialState = new PartialWorldState(0, 0.1, 0);
    // use 10 or 1 second intervals for log / stream data
    this.storage.maxStates = this.sserverLog.isLog() ? 100 : 10;

    this.sserverLog.closed = this.parseBody();

    // return simulation
    return this.sserverLog;
  }

  private async parseBody()
  {
    // console.log('Parsing ulg log body...');
    if (!this.reader || !this.sserverLog || !this.storage) {
      throw new ParserError('Parser not initialized. Need to parse the header first!');
    }
    
    // temporary yield event loop before beginning actual paring
    await aSleep();

    while (true) {
      const { done, value } = await this.reader.read();

      if (done) {
        break;
      }

      try {
        if (value.slice(0, 14) === '(server_param ') {
          ULGParser.parseServerParamLine(value, this.sserverLog, this.storage);
        } else if (value.slice(0, 14) === '(player_param ') {
          ULGParser.parsePlayerParamLine(value, this.sserverLog);
        } else if (value.slice(0, 13) === '(player_type ') {
          ULGParser.parsePlayerTypeLine(value, this.sserverLog);
        } else if (value.slice(0, 6) === '(team ') {
          ULGParser.parseTeamLine(value, this.sserverLog, this.storage);
        } else if (value.slice(0, 10) === '(playmode ') {
          ULGParser.parsePlaymodeLine(value, this.sserverLog, this.storage);
        } else if (value.slice(0, 5) === '(msg ') {
          ULGParser.parseMessageLine(value, this.sserverLog);
        } else if (value.slice(0, 6) === '(draw ') {
          ULGParser.parseDrawLine(value, this.sserverLog);
        } else if (value.slice(0, 6) === '(show ') {
          ULGParser.parseShowLine(value, this.sserverLog, this.storage);
        } else {
          console.log('Unknown ulg log line: ' + value);
        }
      } catch (ex) {
      }

      if (this.storage.hasReachedStateLimit()) {
        this.sserverLog.update(this.storage.consumeStateBuffer(), true);

        // temporary yield event loop
        await aSleep();
      }
    }

    // push final states
    this.storage.storePartialState();
    this.sserverLog.update(this.storage.consumeStateBuffer(), false);

    // reset
    this.reader.releaseLock();
    this.reader = undefined;
    this.sserverLog = undefined;
    this.storage = undefined;
  }








  // ============================================================================
  // ============================== STATIC MEMBERS ==============================
  // ============================================================================

  /**
   * Check if the given url/path/file references a known sserver log file ending.
   *
   * @param url the url to check
   * @returns true, if the given url references a known sserver log file ending, false otherwise
   */
  static isSServerLogFile (url: string): boolean {
    return url.endsWith('.rcg') || url.endsWith('.rcg.gz')
  }

  /**
   * @param line the playmode line
   * @param sserverLog the ssserver log
   * @param storage the parser storage instance
   */
  static parsePlaymodeLine (line: string, sserverLog: SServerLog, storage: ParserStorage): void
  {
    if (!storage.partialState) {
      // Need a partial state first
      return;
    }

    // (playmode <gameTime> <playmode>)
    const rootNode = SymbolTreeParser.parse(line);

    if (rootNode.values.length > 2) {
      const gameTime = parseInt(rootNode.values[1], 10) / 10;

      // Add partial state if valid
      storage.storePartialState();

      // Update partial word state
      storage.partialState.setGameTime(gameTime);
      storage.partialState.setPlaymode(copyString(rootNode.values[2]));
    } else {
      console.log('Invalid playmode line: ' + line);
    }
  }

  /**
   * @param line the team line
   * @param sserverLog the ssserver log
   * @param storage the parser storage instance
   */
  static parseTeamLine (line: string, sserverLog: SServerLog, storage: ParserStorage): void
  {
    if (!storage.partialState) {
      // Need a partial state first
      return;
    }

    // (team <time> <left-team-name> <right-team-name> <goals-left> <goals-right> [<pen-score-left> <pen-miss-left> <pen-score-right> <pen-miss-right>])
    const rootNode = SymbolTreeParser.parse(line);

    if (rootNode.values.length > 5) {
      const gameTime = parseInt(rootNode.values[1], 10) / 10;
      const goalsLeft = parseInt(rootNode.values[4], 10);
      const goalsRight = parseInt(rootNode.values[5], 10);
      let penScoreLeft = 0;
      let penMissLeft = 0;
      let penScoreRight = 0;
      let penMissRight = 0;

      if (rootNode.values.length > 9) {
        penScoreLeft = parseInt(rootNode.values[6], 10);
        penMissLeft = parseInt(rootNode.values[7], 10);
        penScoreRight = parseInt(rootNode.values[8], 10);
        penMissRight = parseInt(rootNode.values[9], 10);
      }

      // Update left team name
      sserverLog.leftTeam.name = copyString(rootNode.values[2]);
      sserverLog.rightTeam.name = copyString(rootNode.values[3]);

      // Add partial state if valid
      storage.storePartialState();

      // Update partial word state
      storage.partialState.setGameTime(gameTime);
      storage.partialState.setScore(goalsLeft, goalsRight, penScoreLeft, penMissLeft, penScoreRight, penMissRight);
    } else {
      console.log('Invalid team line: ' + line);
    }
  }

  /**
   * @param line the show line
   * @param sserverLog the ssserver log
   * @param storage the parser storage instance
   */
  static parseShowLine (line: string, sserverLog: SServerLog, storage: ParserStorage): void
  {
    if (!storage.partialState) {
      // Need a partial state first
      return;
    }

    // (show <time>
    //    [(pm <playmode-no>)]
    //    [(tm <left-team-name> <right-team-name> <goals-left> <goals-right> [<pen-score-left> <pen-miss-left> <pen-score-right> <pen-miss-right>])]
    //    ((b) <x> <y> <vx> <vy>)
    //    [((<side> <unum>) <player_type> <flags> <x> <y> <vx> <vy> <body> <neck> [<point-x> <point-y>]
    //        (v <view-quality> <view-width>)
    //        (s <stamina> <effort> <recovery> [<capacity>])
    //        [(f <side> <unum>)]
    //        (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
    //    )]*
    // )
    const rootNode = SymbolTreeParser.parse(line);

    if (rootNode.values.length > 1) {

      // Add partial state if valid
      storage.storePartialState();
      storage.partialState.setGameTime(parseInt(rootNode.values[1], 10) / 10);

      let childNode;

      // Parse symbol tree into partial world state of sserver log
      for (let i = 0; i < rootNode.children.length; i++) {
        childNode = rootNode.children[i];

        if (childNode.children.length > 0) {
          // Either a ball or player info
          if (childNode.children[0].values[0] === 'b') {
            // Found ball info
            ULGParser.parseBallState(childNode, storage.partialState);
          } else if (childNode.children[0].values[0] === 'l') {
            // Found left team player
            ULGParser.parseAgentState(childNode, sserverLog, sserverLog.leftTeam, storage.partialState.leftAgentStates);
          } else if (childNode.children[0].values[0] === 'r') {
            // Found right team player
            ULGParser.parseAgentState(childNode, sserverLog, sserverLog.rightTeam, storage.partialState.rightAgentStates);
          } else {
            console.log('Found unexpected node in show line: ' + line.slice(0, 20));
          }
        } else if (childNode.values.length > 0) {
          // Either a playmode or team info
          if (childNode.values[0] === 'pm') {
            // parse the playmode number
            console.log('Found pm info in show line...');
          } else if (childNode.values[0] === 'tm') {
            // parse the team and scoring information
            console.log('Found tm info in show line...');
          } else {
            console.log('Found unexpected node in show line: ' + line.slice(0, 20));
          }
        } else {
          console.log('Found empty node in show line: ' + line.slice(0, 20));
        }
      }
    } else {
      console.log('Invalid show line: ' + line.slice(0, 20));
    }
  }

  /**
   * [parseBallState description]
   *
   * @param node the ball symbol node
   * @param partialState the partial world state
   */
  static parseBallState (node: SymbolNode, partialState?: PartialWorldState): void
  {
    // ((b) <x> <y> <vx> <vy>)
    if (!partialState || node.values.length < 2) {
      // Not enough data!
      return;
    }

    partialState.ballState = new ObjectState({
        x: parseFloat(node.values[0]),
        y: parseFloat(node.values[1]),
        z: 0.2,
        qx: 0,
        qy: 0,
        qz: 0,
        qw: 1
      });
  }

  /**
   * [parseAgentState description]
   *
   * @param node the ball symbol node
   * @param sserverLog the ssserver log
   * @param teamDescription the team description
   * @param teamStates the team agent states list
   */
  static parseAgentState (node: SymbolNode, sserverLog: SServerLog, teamDescription: TeamDescription, teamStates: Array<AgentState | undefined>): void
  {
    // ((<side> <unum>) <player_type> <flags> <x> <y> <vx> <vy> <body> <neck> [<point-x> <point-y>]
    //   (v <view-quality> <view-width>)
    //   (s <stamina> <effort> <recovery> [<capacity>])
    //   [(f <side> <unum>)]
    //   (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
    // )

    if (node.values.length < 7) {
      // Invalid agent node
      console.log('Expected more values in agent node: ' + node.values);
      return;
    }

    const playerNo = parseInt(node.children[0].values[1], 10);
    const typeIdx = parseInt(node.values[0], 10);
    const flags = parseInt(node.values[1], 16);

    teamDescription.addAgent(playerNo, sserverLog.playerTypes[typeIdx]);

    // Parse player state data
    let quat: IQuaternion;
    const jointData:number[] = [];
    const agentData:number[] = [];

    let angle = 0;

    angle = parseFloat(node.values[6]);
    quat = JsMath.get2DQuaternion(JsMath.toRad(angle));

    if (node.values.length > 7) {
      angle = parseFloat(node.values[7]);
      jointData[0] = JsMath.toRad(-angle);
    }

    // TODO: Parse stamina, focus and count information
    for (let i = 1; i < node.children.length; i++) {
      const values = node.children[i].values;

      if (values.length > 0) {
        if (values[0] === 'v') {
          // Parse view info
          // (v <view-quality> <view-width>)
          continue;
        } else if (values[0] === 's') {
          // Parse stamina info
          // (s <stamina> <effort> <recovery> [<capacity>])
          if (values.length > 1) {
            agentData[Agent2DData.STAMINA] = parseFloat(values[1]);
          }
          if (values.length > 2) {
            agentData[Agent2DData.STAMINA_EFFORT] = parseFloat(values[2]);
          }
          if (values.length > 3) {
            agentData[Agent2DData.STAMINA_RECOVERY] = parseFloat(values[3]);
          }
          if (values.length > 4) {
            agentData[Agent2DData.STAMINA_CAPACITY] = parseFloat(values[4]);
          }
        } else if (values[0] === 'f') {
          // Parse focus info
          // (f <side> <unum>)
          if (values.length > 2) {
            // TODO: Ensure that everything is expecting a number instead of 'l' and 'r' strings!
            // agentData[Agent2DData.FOCUS_SIDE] = values[1] === 'l' ? 'l' : 'r';
            agentData[Agent2DData.FOCUS_SIDE] = values[1] === 'l' ? -1 : 1;
            agentData[Agent2DData.FOCUS_UNUM] = parseInt(values[2], 10);
          } else {
            console.log('Found unexpected focus node in agent node!');
          }
        } else if (values[0] === 'c') {
          // Parse count info
          // (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
          if (values.length > 1) {
            agentData[Agent2DData.KICK_COUNT] = parseInt(values[1], 10);
          }
          if (values.length > 2) {
            agentData[Agent2DData.DASH_COUNT] = parseInt(values[2], 10);
          }
          if (values.length > 3) {
            agentData[Agent2DData.TURN_COUNT] = parseInt(values[3], 10);
          }
          if (values.length > 4) {
            agentData[Agent2DData.CATCH_COUNT] = parseInt(values[4], 10);
          }
          if (values.length > 5) {
            agentData[Agent2DData.MOVE_COUNT] = parseInt(values[5], 10);
          }
          if (values.length > 6) {
            agentData[Agent2DData.TURN_NECK_COUNT] = parseInt(values[6], 10);
          }
          if (values.length > 7) {
            agentData[Agent2DData.VIEW_COUNT] = parseInt(values[7], 10);
          }
          if (values.length > 8) {
            agentData[Agent2DData.SAY_COUNT] = parseInt(values[8], 10);
          }
          if (values.length > 9) {
            agentData[Agent2DData.TACKLE_COUNT] = parseInt(values[9], 10);
          }
          if (values.length > 10) {
            agentData[Agent2DData.POINT_TO_COUNT] = parseInt(values[10], 10);
          }
          if (values.length > 11) {
            agentData[Agent2DData.ATTENTION_COUNT] = parseInt(values[11], 10);
          }
        } else {
          // Unknown subnode
          console.log('Found unexpected child node in agent node!');
        }
      } else {
        console.log('Found unexpected child node in agent node!');
      }
    }

    teamStates[playerNo] = new AgentState({
        modelIdx: teamDescription.getRecentTypeIdx(playerNo),
        flags: flags,
        x: parseFloat(node.values[2]),
        y: parseFloat(node.values[3]),
        z: 0,
        qx: quat.x,
        qy: quat.y,
        qz: quat.z,
        qw: quat.w,
        jointAngles: jointData,
        data: agentData
      });
  }

  /**
   * @param line the server params line
   * @param sserverLog the ssserver log
   * @param storage the parser storage instance
   */
  static parseServerParamLine (line: string, sserverLog: SServerLog, storage: ParserStorage): void
  {
    // (server_param (<name> <value>)*)
    sserverLog.environmentParams.clear();
    ULGParser.parseParameters(line, sserverLog.environmentParams.paramObj, 'server parameter');

    // Update sserver log frequency and partial state time step
    sserverLog.updateFrequency();
    if (storage.partialState) {
      storage.partialState.timeStep = 1 / sserverLog.frequency;
    }
  }

  /**
   * @param line the player params line
   * @param sserverLog the ssserver log
   */
  static parsePlayerParamLine (line: string, sserverLog: SServerLog): void
  {
    // (player_param (<name> <value>)*)
    sserverLog.playerParams.clear();
    ULGParser.parseParameters(line, sserverLog.playerParams.paramObj, 'player parameter');
  }

  /**
   * @param line the player type line
   * @param sserverLog the ssserver log
   */
  static parsePlayerTypeLine (line: string, sserverLog: SServerLog): void
  {
    // (player_type (<name> <value>)*)
    const playerType: ParameterObject = {};
    ULGParser.parseParameters(line, playerType, 'player type');

    const typeIdx = playerType[PlayerType2DParams.ID];
    if (typeof typeIdx === 'number') {
      sserverLog.playerTypes[typeIdx] = new ParameterMap(playerType);
    }
  }

  /**
   * Parse a parameter line.
   *
   * @param line the data line
   * @param params the target parameter object
   * @param context the parameter context (for logging)
   */
  static parseParameters (line: string, params: ParameterObject, context: string): void
  {
    const rootNode = SymbolTreeParser.parse(line);
    let values;

    // Iterate over all param-value child nodes
    for (let i = 0; i < rootNode.children.length; i++) {
      values = rootNode.children[i].values;

      if (values.length < 2) {
        console.log('Malformated name-value pair in ' + context + ' line: ' + rootNode.children[i]);
        continue;
      }

      if (values[1] === 'true') {
        // Parse as boolean value
        params[values[0]] = true;
      } else if (values[1] === 'false') {
        // Parse as boolean value
        params[values[0]] = false;
      } else if (values[1].charAt(0) === '"') {
        // Parse as string value
        params[values[0]] = copyString(values[1].slice(1, -1));
      } else {
        // Try parse as numerical value
        try {
          params[values[0]] = parseFloat(values[1]);
        } catch (ex) {
          // If parsing as numerical values fails, simply copy the whole string value
          params[values[0]] = copyString(values[1]);
        }
      }
    }
  }

  /**
   * @param line the message line
   * @param sserverLog the ssserver log
   */
  static parseMessageLine (line: string, sserverLog: SServerLog): void {}

  /**
   * @param line the draw line
   * @param sserverLog the ssserver log
   */
  static parseDrawLine (line: string, sserverLog: SServerLog): void {}
}

export { ULGParser };