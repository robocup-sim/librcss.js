import { SimulationType, Simulation } from '../../Simulation.js';

/**
 * The SServerLog class definition.
 *
 * The SServerLog is the central class holding a soccer-server 2D simulation information.
 *
 * @author Stefan Glaser
 */
class SServerLog extends Simulation
{
  /** The ulg protocol version. */
  version: number;

  /**
   * SServerLog Constructor
   * Create a new sserver simulation.
   *
   * @param ressource the simulation ressource
   * @param version the ulg protocol version
   */
  constructor (ressource: URL | File, version: number)
  {
    super(ressource, SimulationType.TWOD);

    this.version = version;
  }
}

export { SServerLog };
