import { Simulation } from '../Simulation.js';
import { SimulationClientState, SimulationClientBase } from './SimulationClient.js';
import { UniversalParser } from '../parser/UniversalParser.js';


/**
 * Check if the given file is gzip encoded.
 * 
 * @param file the file to check
 * @returns true, if the file is gzip encoded, false otherwise
 */
function isGZIPFile (file: File): boolean
{
  return file.name.endsWith('.gz') || file.type === 'application/gzip';
}


/**
 * The SimulationLogConnection class definition.
 *
 * @author Stefan Glaser
 */
class SimulationLogClient extends SimulationClientBase
{
  /** The encapsulated simulation log ressource. */
  protected _ressource: string | URL | File;

  /** The abort controller for aborting fetch requests. */
  protected _controller?: AbortController;

  /**
   * Default Constructor.
   * 
   * @param ressource the simulation log ressource (url or file)
   */
  constructor (ressource: string | URL | File)
  {
    super();

    this._ressource = ressource;
    this._controller = undefined;
  }

  /**
   * The simulation log ressource.
   */
  get ressource (): string | URL | File
  {
    return this._ressource;
  }

  /**
   * Load a simulation log from the encapsulated URL or File.
   * 
   * @param options fetch options
   * @returns A promise that will resolve to a simulation log
   */
  async connect (options?: RequestInit): Promise<Simulation>
  {
    // create new simulation promise on demand
    this._newSimLogPromise ??= new Promise<Simulation>(async (resolve, reject) => {
      try {
        this.state = SimulationClientState.CONNECTING;

        // fetch ressource stream
        let stream: ReadableStream<Uint8Array>;

        if (this._ressource instanceof File) {
          stream = isGZIPFile(this._ressource) ? this._ressource.stream().pipeThrough(new DecompressionStream("gzip")) : this._ressource.stream();
        } else {
          this._controller = new AbortController();
          const signal = this._controller.signal;
          
          const response = await fetch(this._ressource, { signal, ...options });
      
          if (!response.ok || !response.body) {
            throw new Error(response.statusText);
          }

          stream = response.body;

          // override potentially relative ressource URL-string with response URL
          this._ressource = new URL(response.url);
        }

        // start parsing the stream
        this._parser = new UniversalParser();
        
        this.state = SimulationClientState.CONNECTED;
        
        // wait for simulation log header to arrive and be successully parsed into a SimulationLog instance
        this._simLog = await this._parser.parse(stream.pipeThrough(new TextDecoderStream()), this._ressource);

        // use "closed" promise of SimulationLog as "disconnect" promise of this connection
        this._disconnectPromise = this._simLog.closed;

        this.state = SimulationClientState.ACTIVE;

        // register disconnect callbacks
        this._disconnectPromise?.then(() => this.onDisconnect(), (err) => this.onDisconnect());

        resolve(this._simLog);
      } catch (err) {
        // disconnect on error
        this.onDisconnect();
        reject(err);
      }
    });

    return this._newSimLogPromise;
  }

  /**
   * Disconnect from the encapsulated ressource.
   */
  async disconnect (): Promise<void>
  {
    this._controller?.abort('Canceled by user interaction');

    return super.disconnect();
  }

  /**
   * Cleanup references and stuff.
   */
  protected onDisconnect (): void
  {
    super.onDisconnect();
    this._controller = undefined;
  }
}

export { isGZIPFile, SimulationLogClient };
