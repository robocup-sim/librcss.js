import { Simulation } from '../Simulation.js';
import { SimulationClientState, SimulationClientBase } from './SimulationClient.js';
import { UniversalParser } from '../parser/UniversalParser.js';


/**
 * Check if the given URL points to a WebSocket.
 * 
 * @param ressource the ressource url to check
 * @returns true, if the url starts with "ws:" or "wss:", false otherwise
 */
function isStreamRessource (ressource: string | URL): boolean
{
  const url = ressource instanceof URL ? ressource.href : ressource;
  return url.startsWith('ws://') || url.startsWith('wss://');
}


/**
 * Stream source object for synchronously publishing messages to an asynchroneous reader.
 * 
 * @author Stefan Glaser
 */
class WebSocketStreamSource<T>
{
  /** The stream controller instance. */
  controller?: ReadableStreamDefaultController<T>;

  start (controller: ReadableStreamDefaultController<T>)
  {
    this.controller = controller;
  }
}


/**
 * The SimulationStreamConnection class definition.
 *
 * @author Stefan Glaser
 */
class SimulationStreamClient extends SimulationClientBase
{
  /** The encapsulated simulation log ressource. */
  protected _url: URL;

  /** The web socket instance. */
  protected _socket?: WebSocket;

  /** The stream data source object for publishing new messages received from the web socket to the async reader. */
  protected _streamSource?: WebSocketStreamSource<string>;

  /** Text decoder for decoding the message stream. */
  protected _decoder?: TextDecoder;


  /**
   * Default constructor.
   * 
   * @param url the websocket URL of the simulation streaming server
   */
  constructor (url: string | URL)
  {
    super();

    if (!isStreamRessource(url)) {
      throw new Error('Expected websocket address!');
    }

    this._url = url instanceof URL ? url : new URL(url);
    this._socket = undefined;
    this._streamSource = undefined;
    this._decoder = undefined;
  }

  /**
   * The WebSocket stream URL.
   */
  get ressource (): URL
  {
    return this._url;
  }

  /**
   * Connect to the encapsulated websocket stream.
   */
  async connect (): Promise<Simulation>
  {
    // create new simulation promise on demand
    this._newSimLogPromise ??= new Promise<Simulation>(async (resolve, reject) => {
      this.state = SimulationClientState.CONNECTING;

      try {
        // create input stream reader for reading web socket messages asynchronously
        this._streamSource = new WebSocketStreamSource<string>();
        
        // create WebSocket request
        this._socket = new WebSocket(this._url);
        this._socket.addEventListener('open', (event) => this.wsOnOpen(event));
        this._socket.addEventListener('close', (event) => this.wsOnClose(event));
        this._socket.addEventListener('message', (event) => this.wsOnMessage(event));
        this._socket.addEventListener('error', (event) => this.wsOnError(event));

        // this._parser = new ULGParser();
        this._parser = new UniversalParser();
        this._simLog = await this._parser.parse(new ReadableStream<string>(this._streamSource), this._url);;

        // use "closed" promise of SimulationLog as "disconnect" promise of this connection
        this._disconnectPromise = this._simLog.closed;

        this.state = SimulationClientState.ACTIVE;

        resolve(this._simLog);
      } catch (err) {
        // disconnect on errror
        this.onDisconnect();
        reject(err);
      }
    });

    return this._newSimLogPromise;
  }

  /**
   * Disconnect from the websocket.
   */
  async disconnect (): Promise<void>
  {
    this._socket?.close();

    return super.disconnect();
  }

  /**
   * Cleanup references and stuff.
   */
  protected onDisconnect (): void
  {
    // close stream source controller if existing to signal the end of the stream
    this._streamSource?.controller?.close();

    super.onDisconnect();

    this._socket = undefined;
    this._streamSource = undefined;
    this._decoder = undefined;
  }

  /**
   * The WebSocket open callback.
   *
   * // ANYFIX: Should be "Event", but then a lot of stuff is not specified...
   * @param  event the xhr event
   */
  wsOnOpen (event: Event): void
  {
    // console.log(event);

    this.state = SimulationClientState.CONNECTED;
  }

  /**
   * The WebSocket close callback.
   *
   * // ANYFIX: Should be "Event", but then a lot of stuff is not specified...
   * @param  event the xhr event
   */
  wsOnClose (event: CloseEvent): void
  {
    // console.log(event);

    this.onDisconnect();
  }

  /**
   * The WebSocket message callback.
   *
   * // ANYFIX: Should be "Event", but then a lot of stuff is not specified...
   * @param  event the xhr event
   */
  wsOnMessage (event: MessageEvent<any>): void
  {
    // console.log(event);

    let gameInfo = event.data as string;

    // decode message in case it is binary encoded
    if (event.data instanceof ArrayBuffer) {
      this._decoder ??= new TextDecoder('UTF-8');
      gameInfo = this._decoder.decode(event.data);
    }

    // console.log(gameInfo);

    this._streamSource?.controller?.enqueue(gameInfo + '\n');
  }

  /**
   * The WebSocket error callback.
   *
   * @param event the xhr event
   */
  wsOnError (event: Event): void
  {
    // console.log(event);

    this.onDisconnect();
  }
}

export { isStreamRessource, WebSocketStreamSource, SimulationStreamClient };
