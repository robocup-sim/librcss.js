import { Simulation } from '../Simulation.js';
import { ISignal, Signal } from '../utils/Signals.js';
import { IMonitorProtocolParser } from '../parser/MonitorProtocolParser.js';


/**
 * Enum, specifying the different states a simulation connection traverses through its lifespan.
 */
const enum SimulationClientState
{
  /** The initial state of the simulation client after creation. */
  INITIAL = 0,

  /** The simulation client is trying to establish a connection to the specified ressource. */
  CONNECTING = 1,

  /** The client is connected to the simulation at the specified ressource, but has not received any data, yet. */
  CONNECTED = 2,

  /** The simulation client is active, meaning that it received at least a simulation header. */
  ACTIVE = 3,

  /** The connection to the simulation has been closed (the client is disconnected), either because the simulation has ended or because of an internal error. */
  DISCONNECTED = 4
}


/**
 * Common interface for all simulation clients.
 * 
 * @author Stefan Glaser
 */
interface ISimulationClient
{
  /** The ressource associated with this simulation client. */
  readonly ressource: string | URL | File;

  /** The state of the simulation client. */
  readonly state: SimulationClientState;

  /** Signal emitted when the state of the simulation client has changed. */
  readonly onStateChange: ISignal<SimulationClientState>;

  /**
   * Connect to the encapsulated resource.
   * 
   * Multiple calls to this method will return the same promise.
   * So use this method instead of getSimulation to asynchroneously fetch the simulation instance.
   * 
   * @return a promise resolving to a simulation log instance once an connection to an active simulation could be established
   */
  connect (): Promise<Simulation>;

  /**
   * Disconnect form the encapsulated resource.
   * 
   * @return a promise resolving when the client sucessfully disconnected
   */
  disconnect (): Promise<void>;
  
  /**
   * Retrieve the current simulation log if existing.
   *
   * @return the current simulation log if existing, undefined otherwise
   */
  getSimulation (): Simulation | undefined;
}


/**
 * Common base class for components that provide a connection to a simulation.
 *
 * @author Stefan Glaser
 */
abstract class SimulationClientBase implements ISimulationClient
{
  /** Client state. */
  protected _state: SimulationClientState;

  /** The simulation log instance, parsed from the specified simulation log ressource. */
  protected _simLog?: Simulation;

  /** The parser instance. */
  protected _parser?: IMonitorProtocolParser;

  /** Promise resolving to a SimulationLog when a connection to an active simulation could be established. */
  protected _newSimLogPromise?: Promise<Simulation>;

  /** Finished promise. */
  protected _disconnectPromise?: Promise<void>;

  /** Signal emitted when a new "connection" has been astablished. */
  protected _onStateChange: Signal<SimulationClientState>;


  constructor()
  {
    this._state = SimulationClientState.INITIAL;
    this._simLog = undefined;
    this._parser = undefined;
    this._newSimLogPromise = undefined;
    this._disconnectPromise = undefined;

    this._onStateChange = new Signal<SimulationClientState>();
  }

  abstract get ressource (): string | URL | File;

  get state (): SimulationClientState
  {
    return this._state;
  }

  /**
   * Set the current client state, publishing state changes accordingly.
   */
  protected set state (state: SimulationClientState)
  {
    if (this._state !== state) {
      this._state = state;

      // publish state chage
      this._onStateChange.emit(this._state);
    }
  }

  get onStateChange (): ISignal<SimulationClientState>
  {
    return this._onStateChange;
  }

  abstract connect (): Promise<Simulation>;

  async disconnect (): Promise<void>
  {
    if (!!this._disconnectPromise) {
      try {
        await this._disconnectPromise;
      } catch (err) {}
    }
  }

  getSimulation (): Simulation | undefined
  {
    return this._simLog;
  }

  /**
   * Cleanup references and stuff.
   */
  protected onDisconnect (): void
  {
    this.state = SimulationClientState.DISCONNECTED;
    this._parser = undefined;
  }
};

export { SimulationClientState, ISimulationClient, SimulationClientBase };
