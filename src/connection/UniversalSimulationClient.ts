import { Simulation } from '../Simulation.js';
import { ISimulationClient, SimulationClientState } from './SimulationClient.js';
import { isStreamRessource, SimulationStreamClient } from './SimulationStreamClient.js';
import { SimulationLogClient } from './SimulationLogClient.js';
import { ISignal } from '../utils/Signals.js';


/**
 * Interface for simulation client factories, associating ressources with specific clients.
 * 
 * @author Stefan Glaser
 */
interface ISimulationClientFactory
{
  /**
   * Check if this factory provides a client implementation for the given ressource.
   * 
   * @param ressource the ressource to check
   * @returns true, if this factory provides a client implementation for the given ressource, false otherwise
   */
  accepts (ressource: string | URL | File): boolean;

  /**
   * Create a new simulation client instance for the given ressource.
   * 
   * Note: To prevent this method from throwing errors, check for compatibility using the {@link accepts} metod before.
   * 
   * @param ressource the ressource of interest
   */
  create (ressource: string | URL | File): ISimulationClient;
}


/**
 * Default (fallback) factory for internal client implementations.
 * 
 * @author Stefan Glaser
 */
class DefaultSimulationClientFactory implements ISimulationClientFactory
{
  accepts (ressource: string | URL | File): boolean
  {
    // fallback: accept everything by default and throw errors later
    return true;
  }

  create (ressource: string | URL | File): ISimulationClient
  {
    // create client based on ressource type
    return (ressource instanceof File || !isStreamRessource(ressource)) ? new SimulationLogClient(ressource) : new SimulationStreamClient(ressource);
  }
}


/**
 * Create a new Simulation client for the given ressource.
 * 
   @param ressource the simulation ressource (url or file)
 * @returns a new simulation client for the given ressource
 */
function createSimClient (ressource: string | URL | File): ISimulationClient
{
  return UniversalSimulationClient.createSimClient(ressource);
}


/**
 * The UniversalSimulationCLient class definition.
 *
 * @author Stefan Glaser
 */
class UniversalSimulationClient implements ISimulationClient
{
  /** The actual client instance. */
  protected _client: ISimulationClient;

  /** List of client factories. */
  protected static FACTORIES: Array<ISimulationClientFactory> = [ new DefaultSimulationClientFactory() ];

  /**
   * Default Constructor.
   * 
   * @param ressource the simulation ressource (url or file)
   */
  constructor (ressource: string | URL | File)
  {
    this._client = createSimClient(ressource);
  }

  /**
   * The simulation log ressource.
   */
  get ressource (): string | URL | File
  {
    return this._client.ressource;
  }

  get state (): SimulationClientState
  {
    return this._client.state;
  }

  get onStateChange (): ISignal<SimulationClientState>
  {
    return this._client.onStateChange;
  }

  async connect (): Promise<Simulation>
  {
    return this._client.connect();
  }

  async disconnect (): Promise<void>
  {
    return this._client.disconnect();
  }

  getSimulation (): Simulation | undefined
  {
    return this._client.getSimulation();
  }

  /**
   * Register a simulation client factory.
   * 
   * @param factory the factory to register
   */
  static registerClientFactory (factory: ISimulationClientFactory): void
  {
    if (UniversalSimulationClient.FACTORIES.findIndex(f => f === factory) === -1) {
      UniversalSimulationClient.FACTORIES.push(factory);
    }
  }

  /**
   * Unregister a simulation client factory.
   * 
   * @param factory the factory to unregister
   */
  static unregisterClientFactory (factory: ISimulationClientFactory): void
  {
    UniversalSimulationClient.FACTORIES = UniversalSimulationClient.FACTORIES.filter(f => f !== factory);
  }

  /**
   * Create a new Simulation client for the given ressource.
   * 
     @param ressource the simulation ressource (url or file)
   * @returns a new simulation client for the given ressource
   */
  static createSimClient (ressource: string | URL | File): ISimulationClient
  {
    const factories = UniversalSimulationClient.FACTORIES;

    // run backwards trough the factories to prioritize user registered client implementations
    for (let i = factories.length - 1; i >= 0; i--) {
      if (factories[i].accepts(ressource)) {
        return factories[i].create(ressource);
      }
    }

    // no factory is able to handle the ressource
    throw Error('No client implementation for given ressource!');
  }
}

export { ISimulationClientFactory, DefaultSimulationClientFactory, createSimClient, UniversalSimulationClient };
