/**
 * The GameState class definition.
 *
 * The GameState provides information about the current state of a game.
 *
 * @author Stefan Glaser
 */
class GameState
{
  /** The global time when this state was reached. */
  readonly time: number;

  /** The play mode string. */
  readonly playMode: string;


  /** Constant unknown game state. */
  static readonly UNKNOWN = new GameState(0, 'UNKNOWN');

  
  /**
   * GameState Constructor
   * Create a new GameState holding the game state information.
   *
   * @param time
   * @param playMode
   */
  constructor (time: number, playMode: string)
  {
    this.time = time;
    this.playMode = playMode;
  }
}

export { GameState };
