import { AgentDescription } from './AgentDescription.js';
import { ParameterMap } from './utils/ParameterMap.js';
import { ISignal, Signal } from './utils/Signals.js';
import { TeamSide, getTeamSideLetterFor } from './TeamSide.js';


/**
 * The TeamDescription class definition.
 *
 * The TeamDescription provides information about a team.
 *
 * @author Stefan Glaser
 */
class TeamDescription
{
  /** Signal emitted when the name of the team has been changed / updated. */
  private _onNameChange: Signal<string>;
  
  /** Signal emitted when the color of the team has been changed / updated. */
  private _onColorChange: Signal<string>;

  /** Signal emitted when the agent descriptions of the team has been changed / updated. */
  private _onAgentsChange: Signal<void>;

  /** The team side (see TeamSide) */
  readonly side: TeamSide;

  /** The name of the team. */
  private _name: string;

  /** The team color. */
  private _color: string;

  /** The list of agents playing for this team. */
  private _agents: AgentDescription[];

  /**
   * TeamDescription Constructor
   * Create a new TeamDescription with the given parameters.
   *
   * @param name the name of the team
   * @param color the team color
   * @param side the team side (see TeamSide)
   */
  constructor (name: string, color: string, side: TeamSide)
  {
    this._onNameChange = new Signal<string>();
    this._onColorChange = new Signal<string>();
    this._onAgentsChange = new Signal<void>();

    this.side = side;
    this._name = name;
    this._color = color;
    this._agents = [];
  }

  /**
   * Signal emitted when the name of the team has been changed / updated.
   */
  get onNameChange (): ISignal<string>
  {
    return this._onNameChange;
  }
  
  /**
   * Signal emitted when the color of the team has been changed / updated.
   */
  get onColorChange (): Signal<string>
  {
    return this._onColorChange;
  }

  /**
   * Signal emitted when the agent descriptions of the team has been changed / updated.
   */
  get onAgentsChange (): Signal<void>
  {
    return this._onAgentsChange;
  }

  /**
   * Set the name of this team.
   *
   * @param newName the new name of the team
   */
  set name (newName: string)
  {
    newName = newName.split('_').join(' ');

    if (this._name !== newName) {
      this._name = newName;

      // Publish name change event
      this._onNameChange.emit(this._name);
    }
  }

  /**
   * Retrieve the name of this team.
   * @returns the team name
   */
  get name (): string
  {
    return this._name;
  }

  /**
   * Set the color of this team.
   *
   * @param color the new color of the team
   * @returns true, if the color was updated, false otherwise
   */
  set color (color: string)
  {
    if (this._color !== color) {
      this._color = color;

      // Publish color change event
      this._onColorChange.emit(this.color);
    }
  }

  /**
   * Retrieve the color of the team.
   * @returns the team color
   */
  get color (): string
  {
    return this._color;
  }

  /**
   * Retrieve the list of agent discriptions representing this team.
   * @returns the agent descriptions respesenting this team
   */
  get agents (): AgentDescription[]
  {
    return this._agents;
  }

  /**
   * Add an agent description for the given player number and robot model.
   * If there doesn't exist an agent description to the given player number,
   * this method will create a new agent description with the given player number and robot model.
   * If there already exists an agent description with the given player number,
   * this method will add the given robot model to the agent description if it is not yet present.
   *
   * @param number the agent player number
   * @param playerType the player type
   * @returns false if nothing was modified, true otherwise
   */
  addAgent (number: number, playerType: ParameterMap): boolean
  {
    let i = this._agents.length;

    // Check if there already exists a agent description with the given number
    while (i--) {
      if (this._agents[i].playerNo === number) {
        // Add the given player type to the agent
        if (this._agents[i].addPlayerType(playerType)) {
          // Publish agents change event
          this._onAgentsChange.emit();
          return true;
        } else {
          return false;
        }
      }
    }

    // If no agent definition was found for the given player number, create a new one
    this._agents.push(new AgentDescription(number, this.side, playerType));

    // Publish agents change event
    this._onAgentsChange.emit();

    return true;
  }

  /**
   * Retrieve the index of the last used player type specification of the agent with the given player number.
   *
   * @param  playerNo the player number of the agent of interest
   * @returns the index of the last used player type specification
   */
  getRecentTypeIdx (playerNo: number): number
  {
    let i = this.agents.length;

    // Retrieve the requested index from the agent description if existing
    while (i--) {
      if (this.agents[i].playerNo === playerNo) {
        return this.agents[i].recentTypeIdx;
      }
    }

    // return zero by default, if no corresponding agent description was found
    return 0;
  }

  /**
   * Retrieve a letter representing the side.
   *
   * @param uppercase true for upper case letter, false for lower case
   * @returns 'l'/'L' for left side, 'r'/'R' for right side and 'n'/'N' for neutral
   */
  getSideLetter (uppercase: boolean = false): string
  {
    return getTeamSideLetterFor(this.side, uppercase);
  }
}

export { TeamDescription };
