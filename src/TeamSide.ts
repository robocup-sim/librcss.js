/**
 * An enum for the side of a team.
 *
 * @author Stefan Glaser
 */
const enum TeamSide
{
  /** The left team side. */
  LEFT = -1,

  /** The neutral or unknown team side. */
  NEUTRAL = 0,

  /** The right team side. */
  RIGHT = 1
}


/**
 * Retrieve a letter representing the given side.
 *
 * @param side the side value
 * @param uppercase true for upper case letter, false for lower case
 * @returns 'l'/'L' for left side, 'r'/'R' for right side, 'n'/'N' for neutral
 */
function getTeamSideLetterFor (side: TeamSide, uppercase: boolean = false): string
{
  if (uppercase) {
    return side === TeamSide.LEFT ? 'L' : side === TeamSide.RIGHT ? 'R' : 'N';
  } else {
    return side === TeamSide.LEFT ? 'l' : side === TeamSide.RIGHT ? 'r' : 'n';
  }
}

export { TeamSide, getTeamSideLetterFor };
