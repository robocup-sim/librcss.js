import { GameScore } from './GameScore.js';
import { GameState } from './GameState.js';
import { ParameterMap } from './utils/ParameterMap.js';
import { TeamSide } from './TeamSide.js';
import { TeamDescription } from './TeamDescription.js';
import { WorldState } from './WorldState.js';
import { SparkUtil, Environment3DParams } from './utils/SparkUtil.js';
import { SServerUtil, Environment2DParams } from './utils/SServerUtil.js';
import { ISignal, Signal } from './utils/Signals.js';


/**
 * Enum holding the known simulation types.
 * 
 * @author Stefan Glaser
 */
const enum SimulationType
{
  /** 2D simulation type (SServer). */
  TWOD = 0,

  /** 3D simulation type (SimSpark). */
  THREED = 1
}


/**
 * Enum for holding the different ressource types a simulation may have.
 * 
 * @author Stefan Glaser
 */
const enum RessourceType
{
  /**
   * A log file ressource results in a simulation that can not be manipulated by the user.
   * It is (typically) fully loaded into memory to allow full navigation fo the simulation state information.
   */
  LOGFILE = 0,

  /**
   * Similar to a log file ressource, a stream ressource results in a simulation that can not be manipulated by the user.
   * However, simulation state information is expected arrive continuously over time until the simulation has finished.
   * Simulation state information is (typically) fully buffered for stream pausing and full navigation of the simulation state information.
   * This type of ressource is intended for streaming (live) games without any interaction with the simulation server that is potentially running somewhere in the network.
   */
  STREAM = 1,

  // /**
  //  * A live monitor ressource will result in a simulation that is fully interactable.
  //  * Simulation state data will arrive continuously over time until the simulation has finished.
  //  * The user can interact with the simulation using monitor commands corresponding to the different simulation types.
  //  * This ressource type will limit the simulation state buffer size to prevent running out of memory while monitoring a simulation.
  //  * The limitation of the buffer size should not be an issue, as this ressource is expected to be played in real-time and thus state navigation is not required.
  //  * 
  //  * Note: Not implemented yet!
  //  */
  // LIVE_MONITOR = 2
}


/**
 * Retrieve the type of given the ressource.
 * 
 * A simulation can be loaded from a log file, (live) streamed via the network or directly received from a simulation server.
 * The ressource type reflects this differend kinds of data origins.
 * 
 * @param ressource the simulation ressource
 * @returns the ressource type
 */
function getRessourceTypeFor (ressource: URL | File): RessourceType
{
  if (ressource instanceof File) {
    // a file ressource can only be a log file
    return RessourceType.LOGFILE;
  } else {
    const protocol = ressource.protocol;

    if (protocol === 'ws:' || protocol === 'wss:') {
      // web socket ressource --> stream
      return RessourceType.STREAM;
    // } else if (protocol === 'tcp:') {
      // not sure about that, yet, as "tcp" is not an official URL protocol and thus not well supported by browsers
      // return RessourceType.LIVE_MONITOR;
    } else {
      return RessourceType.LOGFILE;
    }
  }
}


/**
 * The (Soccer) Simulation class definition.
 *
 * The Simulation is the central class holding information about a simulation.
 * A Simulation may represent a full log / replay file of a recorded simulation, but can also be used to buffer simulation states received by a (live) stream or live monitor connection.
 *
 * @author Stefan Glaser
 */
class Simulation
{
  // /** Signal emitted when the environemnt parameter have been changed / updated. */
  // protected _onEnvironmentChanged: Signal<void>;

  /** Signal emitted when the list of states has been modified (new states added / existing states removed). */
  protected _onStatesChanged: Signal<void>;

  /** The simulation log / replay file or streaming url. */
  readonly ressource: URL | File;

  /** The type of ressource associated with this simulation. */
  readonly ressourceType: RessourceType;

  /** The simulation type (2D or 3D). */
  readonly type: SimulationType;

  /** The state update frequency of the simulation. */
  frequency: number;

  /** The list of server/simulation environment parameters. */
  environmentParams: ParameterMap;

  /** The list of player parameters. */
  playerParams: ParameterMap;

  /** The list of player type parameters. */
  playerTypes: ParameterMap[];

  /** The description of the left team. */
  readonly leftTeam: TeamDescription;

  /** The description of the right team. */
  readonly rightTeam: TeamDescription;

  /** The list of all world states. */
  readonly states: WorldState[];

  /** The time value of the first state. */
  protected _startTime: number;

  /** The time value of the last state. */
  protected _endTime: number;

  /** The duration of the simulation. */
  protected _duration: number;

  /** A list of game states over time. */
  readonly gameStateList: GameState[];

  /** A list of game scores over time. */
  readonly gameScoreList: GameScore[];

  /** Indicator if the connection to the simulation has been closed. */
  protected _isClosed: boolean;

  /** Promise resolving when the connection to the simulation has been closed. */
  closed?: Promise<void>;

  // /** The maximum number of states to buffer, or <=0 for unlimited storage. */
  // protected _bufferSize: number;

  /**
   * Simulation Constructor
   * Create a new simulation.
   * 
   * @param ressource the simulation ressource
   * @param type the simulation type
   */
  constructor (ressource: URL | File, type: SimulationType)
  {
    // this.onEnvironmentChanged = new Signal<void>();
    // this.onPlayerChanged = new Signal<void>();
    this._onStatesChanged = new Signal<void>();

    this.ressource = ressource;
    this.ressourceType = getRessourceTypeFor(ressource);
    this.type = type;
    this.frequency = 1;
    this.environmentParams = new ParameterMap();
    this.playerParams = new ParameterMap();
    this.playerTypes = [];
    this.leftTeam = new TeamDescription('Left Team', '#ffff00', TeamSide.LEFT);
    this.rightTeam = new TeamDescription('Right Team', '#ff0000', TeamSide.RIGHT);
    this.states = [];
    this._startTime = 0;
    this._endTime = 0;
    this._duration = 0;
    this.gameStateList = [];
    this.gameScoreList = [];
    this._isClosed = false;
    this.closed = undefined;
    // this._bufferSize = this.ressourceType === RessourceType.LIVE_MONITOR ? 100 : -1;

    // Create defaults
    if (type === SimulationType.TWOD) {
      this.environmentParams = SServerUtil.createDefaultEnvironmentParams();
      this.playerParams = SServerUtil.createDefaultPlayerParams();
      this.playerTypes = SServerUtil.createDefaultPlayerTypeParams();
    } else {
      this.environmentParams = SparkUtil.createDefaultEnvironmentParams();
      this.playerParams = SparkUtil.createDefaultPlayerParams();
      this.playerTypes = SparkUtil.createDefaultPlayerTypeParams();
    }

    this.updateFrequency();
  }

  // /**
  //  * Signal emitted when the environemnt parameter have been changed / updated.
  //  */
  // get onEnvironmentChanged (): ISignal<void>
  // {
  //   return this._onEnvironmentChanged;
  // }

  /** Signal emitted when the list of states has been modified (new states added / existing states removed). */
  get onStatesChanged (): ISignal<void>
  {
    return this._onStatesChanged;
  }

  /** Retrieve the start time (global time) of the simulation (time of the first state). */
  get startTime (): number
  {
    return this._startTime;
  }

  /** Retrieve the end time (global time) of the simulation (time of the last state). */
  get endTime (): number
  {
    return this._endTime;
  }

  /** Retrieve the duration (global time) of the simulation. */
  get duration (): number
  {
    return this._duration;
  }

  /** Flag if the connection to the simulation has been closed. */
  get isClosed (): boolean
  {
    return this._isClosed;
  }

  // /** Retrieve the state buffer size of this simulation instance (<=0 for unlimited storage). */
  // get bufferSize (): number
  // {
  //   return this._bufferSize;
  // }

  /**
   * Check if this simulation represents a log file ressource.
   * 
   * @returns true, if this simulation represents a log file resource, false otherwise
   */
  isLog (): boolean
  {
    return this.ressourceType === RessourceType.LOGFILE;
  }

  /**
   * Check if this simulation represents a (live) stream.
   * 
   * @returns true, if this simulation represents a (live) stream, false otherwise
   */
  isStream (): boolean
  {
    return this.ressourceType === RessourceType.STREAM;
  }

  // /**
  //  * Check if this simulation represents a live monitor connection.
  //  * 
  //  * @returns true, if this simulation represents a live monitor connection, false otherwise
  //  */
  // isLive (): boolean
  // {
  //   return this.ressourceType === RessourceType.LIVE_MONITOR;
  // }

  /**
   * Check if the ressource of this simulation is a URL.
   * 
   * @returns true, if the ressource is an URL, false otherwise
   */
  isURLRessource (): boolean
  {
    return this.ressource instanceof URL;
  }

  /**
   * Check if the ressource of this simulation is a file.
   * 
   * @returns true, if the ressource is an file, false otherwise
   */
  isFileRessource (): boolean
  {
    return this.ressource instanceof File;
  }

  /**
   * Update the frequency value from environment parameter list.
   */
  updateFrequency (): void
  {
    let step = undefined;

    if (this.type === SimulationType.TWOD) {
      step = this.environmentParams.getNumber(Environment2DParams.SIMULATOR_STEP);
    } else {
      step = this.environmentParams.getNumber(Environment3DParams.LOG_STEP);
    }

    if (step) {
      this.frequency = 1000 / step;
    }
  }

  /**
   * Fetch the index of the world state that corresponds to the given time.
   *
   * @param time the global time
   * @returns the world state index corresponding to the specified time
   */
  getIndexForTime (time: number): number
  {
    const idx = Math.floor(time * this.frequency);

    if (idx < 0) {
      return 0;
    } else if (idx >= this.states.length) {
      return this.states.length - 1;
    }

    return idx;
  }

  /**
   * Retrieve the world state for the given time.
   *
   * @param time the global time
   * @returns the world state closest to the specified time
   */
  getStateForTime (time: number): WorldState | undefined
  {
    return this.states[this.getIndexForTime(time)];
  }

  /**
   * Update the list of world states with new state information.
   * 
   * @param newStates the new world state information
   * @param incremental flag for incremental data (true, if further states may follow, false if not)
   */
  update (newStates: Array<WorldState>, incremental: boolean = true): void
  {
    // updated closed status
    this._isClosed = !incremental;

    const lastState = this.states[this.states.length - 1];
    let previousGameState = lastState?.gameState;
    let previousScore = lastState?.score;
    
    // copy new state information while extracting some statistics
    for (let state of newStates) {
      // add new state to state array
      this.states.push(state);

      // check for game state change
      if (previousGameState !== state.gameState) {
        previousGameState = state.gameState;
        this.gameStateList.push(previousGameState);
      }

      // check for game score change
      if (previousScore !== state.score) {
        previousScore = state.score;
        this.gameScoreList.push(previousScore);
      }
    }

    // update times
    if (this.states.length > 0) {
      this._startTime = this.states[0].time;
      this._endTime = this.states[this.states.length - 1].time;
      this._duration = this._endTime - this._startTime;
    }

    // notify listeners
    this._onStatesChanged.emit();
  }
}

export { SimulationType, RessourceType, Simulation };
